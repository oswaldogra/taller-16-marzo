var superagent = require('superagent')
var expect = require('expect.js')

describe('express rest api server', function () {
  var id
  // GET /suma
  it('sumar 1+3', function (done) {
    superagent.get('http://localhost:3000/suma/?numero1=1&numero2=3')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        expect(res.body.respuesta).to.eql(4)
        done()
      })
  })


})
