

var express = require('express');
var app = express();

app.get('/suma', (req, res) => {
    var numero1 = Number(req.query.numero1) ;
    var numero2 = Number(req.query.numero2) ;
    var resultado = numero1+numero2;
    res.send({respuesta: resultado});
})

app.listen(3000, function () {
  console.log('suma en puerto 3000!');
});